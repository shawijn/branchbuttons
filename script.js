const newButton = document.getElementsByClassName(
  "detail-page-header-actions"
)[0];
newButton.insertAdjacentHTML(
  `afterbegin`,
  `<button type="button" style="margin-left: unset; margin-right: 0.5em;" class="btn gl-display-none gl-sm-display-inline-flex! btn-default gl-ml-3 btn-md gl-button"><span class="gl-button-text">
    Get branch name
    </span></button>`
);
newButton.addEventListener("click", () => {
  let title = document
    .getElementsByClassName("title qa-title")[0]
    .textContent.replace(/[^a-zA-Z\d\s]/, "")
    .replaceAll(" ", "-")
    .toLocaleLowerCase();
  let link = window.location.href.split("/");
  let issueNumber = link[link.length - 1];
  navigator.clipboard.writeText(`#${issueNumber}-${title}`);
});
